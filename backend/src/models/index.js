import 'dotenv/config';
import mongoose from 'mongoose';
import Cryptocurrency from './cryptocurrency';

const connectDb = () => {
  return mongoose.connect(`mongodb://${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`);
};

const models = {
  Cryptocurrency,
};

export { connectDb };
export default models;