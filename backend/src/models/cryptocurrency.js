import mongoose from 'mongoose';

const schema = mongoose.Schema({
  code: String,
  coingeckoId: String,
  canSell: Boolean,
  decimalScale: Number,
});

export default mongoose.model('Cryptocurrency', schema);