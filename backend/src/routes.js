import { Router } from 'express';
import axios from 'axios';
import models from './models';

const router = Router();

router.get('/ping', (req, res) => {
  res.send('pong');
})

// get cryptocurrencies 1-week daily market data against USD
router.post('/cryptocurrencies/market-data', async (req, res) => {
  const fiat = req.body.currency || 'usd';
  const cryptocurrencies = await models.Cryptocurrency.find({ coingeckoId: { $exists: true } }, { _id: 0 });

  Promise.all(cryptocurrencies.map(item => axios.get(`https://api.coingecko.com/api/v3/coins/${item.coingeckoId}/market_chart`, {
    params: {
      'vs_currency': fiat,
      'days': 14,
      'interval': 'daily',
    }
  })))
    .then(values => {
      const marketData = values.map((value, idx) => {
        return {
          ticker: `${cryptocurrencies[idx].code}/${fiat.toUpperCase()}`,
          decimalScale: cryptocurrencies[idx].decimalScale,
          marketData: {
            timestamp: value.data.prices.map(price => price[0]),
            prices: value.data.prices.map(price => price[1]),
          }
        }
      });

      res.json(marketData);
    });
});

// get fiat and crypto currencies
router.get('/trade/options', async (req, res) => {
  Promise.all([
    axios.get('https://xanpool.com/api/methods'),
    models.Cryptocurrency.find({}, { _id: 0, coingeckoId: 0 }),
  ])
    .then(responses => {
      // format fiat currencies
      const fiatCurrencies = responses[0].data.buy.map(currency => {
        const paymentMethods = currency.methods.map(method => {
          return {
            code: method.method,
            label: method.name,
            min: method.min,
            max: method.max,
          };
        });

        const canSell = responses[0].data.sell.find(c => c.currency === currency.currency);

        return {
          code: currency.currency,
          label: currency.currency.toUpperCase(),
          canSell: canSell ? true : false,
          paymentMethods,
        }
      })
        .sort((a, b) => a.label.localeCompare(b.label));

      // format cryptocurrencies
      const cryptoCurrencies = responses[1].map(currency => {
        return {
          code: currency.code,
          label: currency.code.toUpperCase(),
          canSell: currency.canSell,
          decimalScale: currency.decimalScale,
        };
      });

      res.json({
        'fiat': fiatCurrencies,
        'crypto': cryptoCurrencies,
      });
    });
});

// get transaction estimation
router.post('/trade/estimation', async (req, res) => {
  let payload = {
    'type': req.body.type,
    'cryptoCurrency': req.body.crypto,
    'method': req.body.method,
  };

  if (req.body.currency === 'fiat') {
    payload.fiat = req.body.amount;
  } else {
    payload.crypto = req.body.amount;
  }

  axios.post('https://xanpool.com/api/transactions/estimate', payload)
    .then(response => {
      res.json({
        'fiat': response.data.fiat,
        'crypto': response.data.crypto,
      });
    });
});

export default router;