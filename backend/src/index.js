import express from 'express';
import path from 'path';
import cors from 'cors';
import routes from './routes';
import { connectDb } from './models';

const app = express();
const port = 8888;

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(cors({
  origin: [
    'https://api.coingecko.com',
    'https://xanpool.com',
  ],
}));

// serve react app static files
app.use(express.static(path.join(__dirname, '../../web/build')));

// api endpoints
app.use('/api', routes);

// handle unmatched requests to return to react app 
app.get('*', (req, res) => {
  res.status(400).json({
    'statusCode': 400,
    'error': 'Bad Request',
  });
});

connectDb().then(async () => {
  app.listen(port, () => {
    console.log(`Express listening at http://localhost:8888`);
  });
});