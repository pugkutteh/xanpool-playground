module.exports = {
  apps: [{
    name: "xanpool",
    script: "./build/index.js",
    node_args: '-r dotenv/config',
  }]
}