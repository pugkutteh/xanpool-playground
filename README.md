# HODL Playground

Hi there! This is my first attempt at building a web app using Express and MongoDB. Do leave a feedback and help me to improve for the better :)

---

## Stack Used

- React
- Express
- MongoDB

## // TODO:

- GraphQL
- Unit tests? 🙃

---

### Installation

1. Clone the project and install the node package in root folder:

```
npm i
```

2. Install the node packages in `backend` and `web` folders:

```
npm run provision
```

3. Setup environment variables in `./backend/.env`

```
cp ./backend/.env.example ./backend/.env
```

### Development

Run the command below to start a development server. Web app will be accessible at `http://localhost:3000`, while backend at `http://localhost:8888`

```
npm run dev
```

### Production

1. Build static files in `backend` and `web` folders:

```
npm run build
```

2. Serve the static files by running:

```
npm serve
```
