type Props = {
  children: JSX.Element | JSX.Element[],
  classes?: string,
};

const Container = ({ children, classes }: Props) => {
  return (
    <div className={`container max-w-screen-lg px-4 ${classes}`}>
      {children}
    </div>
  )
}

export default Container;