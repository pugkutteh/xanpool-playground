type Props = {
  children: JSX.Element | JSX.Element[],
  classes?: string,
};

const Card = ({ children, classes }: Props) => {
  return (
    <div className={`bg-white rounded-lg shadow-lg ${classes}`}>
      {children}
    </div>
  )
}

export default Card;