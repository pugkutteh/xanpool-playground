type Props = {
  label: string | number,
  classes?: string,
}

const RoundedBadge = ({
  label,
  classes,
}: Props) => (
  <div
    className={`text-xs text-center text-white rounded-full w-4 h-4 inline-block bg-secondary align-text-bottom ${classes}`}
    style={{ paddingTop: "0.13em" }}
  >{label}</div>
)

export default RoundedBadge;