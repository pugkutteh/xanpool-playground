import { DropdownOptionType } from "models/DropdownOption";
import React from "react";

type Props = {
  id: string,
  options: DropdownOptionType[],
  selectedOption?: DropdownOptionType | null,
  placeholder: string,
  onChange: (option: any) => void,
};

const Dropdown = ({
  id,
  options,
  selectedOption,
  placeholder,
  onChange,
}: Props) => {
  const placeholderClasses = selectedOption ? 'text-primary' : 'text-gray-400';

  const handleOnClick = (option: DropdownOptionType) => (e: React.MouseEvent) => {
    // remove focus
    if (document.activeElement instanceof HTMLElement) {
      document.activeElement.blur();
    }

    // invoke callback
    onChange(option);
  }

  return (
    <div className="dropdown">
      <button
        className="w-full flex items-center px-3 py-2 bg-gray-100 rounded-full transition-all duration-300 hover:bg-gray-200 outline-none focus:outline-none active:bg-gray-50 active:text-gray-800"
        type="button"
        aria-haspopup="true"
        aria-expanded="true"
        aria-controls={`dropdown-${id}`}
      >
        <span className={`text-sm text-left flex-grow ${placeholderClasses}`}>{selectedOption ? selectedOption.label : placeholder}</span>
        <svg className="w-5 h-5 ml-2 -mr-1" viewBox="0 0 20 20" fill="currentColor"><path fillRule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>
      </button>

      <div className="opacity-0 invisible dropdown-menu transition-all duration-100">
        <div
          className="absolute right-0 w-full overflow-y-auto mt-2 bg-white border border-gray-200 divide-y divide-gray-100 rounded-md shadow-lg overflow-hidden"
          style={{ maxHeight: 204 }}
          aria-labelledby="headlessui-menu-button-1"
          id={`dropdown-${id}`}
          role="menu"
        >
          {!options.length && <div className="px-4 py-2 text-gray-400 text-sm">No options available</div>}

          {!!options.length && options.map((option, idx) => (
            <button
              key={idx}
              className="w-full px-4 py-2 text-gray-700 transition-all duration-300 hover:bg-gray-50"
              role="menuitem"
              data-tabindex={idx}
              onClick={handleOnClick(option)}
            >{option.label}</button>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Dropdown;