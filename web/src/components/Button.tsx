type Props = {
  classes?: string,
  label: string,
  icon?: JSX.Element,
  iconPosition?: string,
  disabled?: boolean,
  onClick: React.MouseEventHandler,
};

const Button = ({
  classes,
  label,
  icon,
  iconPosition = "left",
  disabled = false,
  onClick,
}: Props) => {
  const iconMargin = iconPosition === "right" ? "ml-2" : "mr-2";
  const Icon = () => <div className={`inline-block w-4 h-4 ${iconMargin}`}>{icon}</div>

  return (
    <button
      className={`bg-gradient text-white rounded-md shadow-lg px-4 py-1.5 flex items-center justify-between transition-all duration-200 hover:opacity-90 disabled:opacity-50 disabled:cursor-not-allowed ${classes}`}
      type="button"
      disabled={disabled}
      onClick={onClick}
    >
      {icon && iconPosition !== "right" && <Icon />}
      <span>{label}</span>
      {icon && iconPosition === "right" && <Icon />}
    </button>
  )
}

export default Button;