import { Link } from "react-router-dom";
import Container from "components/Container";
import { version } from "../../package.json";

const Header = () => {
  return (
    <header>
      <Container>
        <div
          className="flex items-center justify-between"
          style={{ height: 70 }}
        >
          <Link to="/">
            <h1
              className="text-white text-lg font-bold hover:text-opacity-70 duration-300"
              style={{ letterSpacing: 0.3 }}
            >HODL Playground</h1>
          </Link>

          <div className="text-sm text-white">V{version}</div>
        </div>
      </Container>
    </header>
  )
}

export default Header;