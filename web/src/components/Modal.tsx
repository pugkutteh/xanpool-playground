import { useEffect } from "react";
import { useState } from "react";
import ReactDOM from "react-dom";
import Button from "./Button";

type Props = {
  children: JSX.Element | JSX.Element[],
  classes?: string,
  show: boolean,
  toggle: (flag: boolean) => void,
};

const Modal = ({
  children,
  classes = "",
  show,
  toggle,
}: Props) => {
  const [customClasses, setCustomClasses] = useState(`opacity-0 invisible ${classes}`);

  useEffect(() => {
    if (show) {
      setTimeout(() => {
        setCustomClasses(`opacity-1 visible ${classes}`);
      }, 0);
    }
  }, [show, classes]);

  const handleCloseModal = () => {
    setCustomClasses(`opacity-0 invisible ${classes}`);

    setTimeout(() => {
      toggle(false);
    }, 200);
  }

  return show ?
    ReactDOM.createPortal(
      <div
        className={`fixed text-gray-500 flex items-center justify-center overflow-auto z-50 bg-black bg-opacity-70 left-0 right-0 top-0 bottom-0 transition-all duration-150 ${customClasses}`}
        role="dialog"
        aria-modal="true"
      >
        <div className="bg-white rounded-xl shadow-2xl p-6 w-full md:max-w-xl mx-10">
          {children}

          <hr className="my-5" />

          <div className="flex justify-end">
            <Button
              label="Close"
              onClick={handleCloseModal}
            />
          </div>
        </div>
      </div>
      , document.body)
    : null;
}

export default Modal;