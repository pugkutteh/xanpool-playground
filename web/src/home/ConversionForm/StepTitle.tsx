import RoundedBadge from "components/RoundedBadge";

type Props = {
  stepNumber: number | string,
  label: string,
}

const StepTitle = ({ stepNumber, label }: Props) => (
  <div className="text-sm mb-2">
    <RoundedBadge
      classes="mr-1"
      label={stepNumber}
    ></RoundedBadge>
    {label}
  </div>
)

export default StepTitle;