import { SwitchVerticalIcon } from "@heroicons/react/solid";

type Props = {
  onClick: React.MouseEventHandler,
}

const SwitchConversionButton = ({ onClick }: Props) => {
  return (
    <button
      className="absolute w-6 h-6 p-1 rounded-full shadow-lg left-1/2 -ml-3 -bottom-7 transform duration-150 hover:scale-110"
      style={{
        background: "var(--color-main-gradient)",
      }}
      onClick={onClick}
    >
      <SwitchVerticalIcon className="text-white" />
    </button>
  )
}

export default SwitchConversionButton;