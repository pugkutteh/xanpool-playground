import { useEffect, useState } from "react";
import { DropdownOptionType } from "models/DropdownOption";
import Dropdown from "components/Dropdown";

type Props = {
  id: string,
  options: DropdownOptionType[],
  selectedOption: DropdownOptionType,
  value?: number | undefined,
  placeholder?: string,
  min?: number,
  max?: number,
  shouldValidate?: boolean,
  isLoading?: boolean,
  onInputChange: (value: number) => void,
  onDropdownChange: (option: number | undefined) => void,
}

const FormGroup = ({
  id,
  options,
  selectedOption,
  value,
  placeholder = "",
  min,
  max,
  shouldValidate = false,
  isLoading = false,
  onInputChange,
  onDropdownChange,
}: Props) => {
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    validateInputRange(value);
  }, [value]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = parseFloat(e.target.value);

    if (shouldValidate) validateInputRange(value);
    onInputChange(value);
  }

  const validateInputRange = (value: number | undefined) => {
    if (
      value &&
      (
        (min && value < min) ||
        (max && value > max)
      )
    ) {
      setHasError(true);
    } else {
      setHasError(false);
    }
  }

  return (
    <div>
      <div className="flex">

        <div className="flex-grow flex items-center">
          <input
            className="flex-grow w-full p-2 ml-3"
            type="number"
            value={value}
            maxLength={10}
            placeholder={placeholder}
            onChange={handleInputChange}
          />

          {/* loading icon */}
          <svg
            className={`w-8 h-8 mx-1 animate-spin ${isLoading ? 'visible' : 'invisible'}`}
            fill="none"
            viewBox="0 0 32 32"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clipRule="evenodd"
              d="M15.165 8.53a.5.5 0 01-.404.58A7 7 0 1023 16a.5.5 0 011 0 8 8 0 11-9.416-7.874.5.5 0 01.58.404z"
              fill="currentColor"
              fillRule="evenodd"
            />
          </svg>
        </div>

        <Dropdown
          id={id}
          options={options}
          selectedOption={selectedOption}
          placeholder="Choose an option"
          onChange={onDropdownChange}
        ></Dropdown>
      </div>

      {hasError && <div className="text-xs text-red-500 mt-2 ml-5">The limit per transaction is between {min} - {max}</div>}
    </div>
  )
}

export default FormGroup;