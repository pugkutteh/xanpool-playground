import { useState, useEffect, useRef, useMemo } from "react";
import axios from "axios";
import { ArrowNarrowRightIcon } from "@heroicons/react/solid";
import debounce from "lodash.debounce";
import { FiatCurrencyType } from "models/FiatCurrency";
import { CryptoCurrencyType } from "models/CryptoCurrency";
import { PaymentMethodType } from "models/PaymentMethod";
import useModal from "hooks/useModal";
import Dropdown from "components/Dropdown";
import Card from "components/Card";
import Button from "components/Button";
import Modal from "components/Modal";
import StepTitle from "./StepTitle";
import SwitchConversionButton from "./SwitchConversionButton";
import FormGroup from "./FormGroup";

type StateType = {
  tradeType: "buy" | "sell",
  filteredFiatCurrencies: FiatCurrencyType[],
  filteredCryptoCurrencies: CryptoCurrencyType[],
  selectedFiat: FiatCurrencyType,
  selectedCrypto: CryptoCurrencyType,
  selectedPaymentMethod: PaymentMethodType,
};

type InputStateType = {
  fiat: number | undefined,
  crypto: number | undefined,
}

type EstimationPayloadType = {
  type: string,
  currency: string,
  crypto: string,
  amount: number,
  method: string,
};

type UnsupportedConversionType = {
  hasError: boolean,
  currency: string,
  count: number,
};

const ConversionForm = () => {
  const { showModal, toggleModal } = useModal();

  const allCurrencies = useRef({
    "fiat": [],
    "crypto": [],
  });

  const [inputAmount, setInputAmount] = useState<InputStateType>({
    fiat: 0,
    crypto: 0,
  });

  const [unsupportedConversion, setUnsupportedConversion] = useState<UnsupportedConversionType>({
    hasError: false,
    currency: "",
    count: 0,
  });

  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [state, setState] = useState<StateType>({
    tradeType: "buy",
    filteredFiatCurrencies: [],
    filteredCryptoCurrencies: [],
    selectedFiat: {
      code: "",
      label: "",
      canSell: false,
      paymentMethods: [],
    },
    selectedCrypto: {
      code: "",
      label: "",
      canSell: false,
      decimalScale: 0,
    },
    selectedPaymentMethod: {
      code: "",
      label: "",
      min: undefined,
      max: undefined,
    },
  });

  useEffect(() => {
    // get trade options on first load
    axios.get('/api/trade/options')
      .then(response => {
        // fiat
        const fiatCurrencies = response.data.fiat;
        const firstFiat = fiatCurrencies[0] ?? {};
        allCurrencies.current.fiat = fiatCurrencies;
        setInputAmount(state => {
          return {
            ...state,
            fiat: firstFiat.paymentMethods[0].max ?? 0,
          }
        });

        // crypto
        const cryptoCurrencies = response.data.crypto;
        allCurrencies.current.crypto = cryptoCurrencies;

        setState(state => {
          return {
            ...state,
            filteredFiatCurrencies: fiatCurrencies,
            filteredCryptoCurrencies: cryptoCurrencies,
            selectedFiat: firstFiat,
            selectedCrypto: cryptoCurrencies[0] ?? {},
            selectedPaymentMethod: firstFiat.paymentMethods[0] ?? {},
          };
        });
      });
  }, []);

  useEffect(() => {
    getQuote("fiat", "useEffect", inputAmount.fiat);
  }, [state]);

  useEffect(() => {
    if (unsupportedConversion.count > 1) {
      setUnsupportedConversion({
        hasError: false,
        currency: "",
        count: 0,
      });
    }
  }, [unsupportedConversion])

  const getQuote = (currencyType: "fiat" | "crypto", source: string, amount: number | undefined) => {
    if (
      typeof amount === "undefined" ||
      !state.selectedCrypto.code ||
      !state.selectedPaymentMethod.code
    ) return;

    setIsLoading(true);

    let payload: EstimationPayloadType = {
      "type": state.tradeType,
      "currency": currencyType,
      "crypto": state.selectedCrypto.code,
      "amount": amount,
      "method": state.selectedPaymentMethod.code,
    };

    axios.post("/api/trade/estimation", payload)
      .then(response => {
        if (currencyType === "fiat") {
          setInputAmount(state => {
            return {
              ...state,
              crypto: response.data.crypto,
            }
          });

        } else if (currencyType === "crypto") {
          setInputAmount(state => {
            return {
              ...state,
              fiat: response.data.fiat,
            }
          });
        }
      })
      .finally(() => {
        // add timeout to delay in hiding loading icon
        setTimeout(() => {
          setIsLoading(false);
        }, 300);

        // retain unsupported disclaimer until next render
        if (unsupportedConversion.hasError) {
          setUnsupportedConversion(state => {
            return {
              ...state,
              count: state.count + 1,
            };
          });
        }
      });
  }

  const handlePaymentMethodChange = (option: PaymentMethodType) => {
    setState(state => {
      return {
        ...state,
        selectedPaymentMethod: option,
      };
    });
  }

  const handleDropdownChange = (type: string) => (option: any) => {
    if (type === "fiat") {
      setInputAmount(state => {
        return {
          ...state,
          fiat: option.paymentMethods[0].max,
        }
      });

      setState(state => {
        return {
          ...state,
          selectedFiat: option,
          selectedPaymentMethod: option.paymentMethods[0],
        };
      });

    } else if (type === "crypto") {
      setState(state => {
        return {
          ...state,
          selectedCrypto: option,
        };
      });
    }
  }

  // curry function didn't work in debounce
  const handleInputChange = (type: string, value: any) => {
    const amount = isNaN(value) ? undefined : value;

    if (type === "fiat") {
      setInputAmount(state => {
        return {
          ...state,
          fiat: amount,
        }
      });

      if (amount != null) debouncedGetQuote("fiat", "handleInputChange", amount);

    } else if (type === "crypto") {
      setInputAmount(state => {
        return {
          ...state,
          crypto: amount,
        }
      });

      if (amount != null) debouncedGetQuote("crypto", "handleInputChange", amount);
    }
  }

  const debouncedGetQuote = useMemo(() => debounce(getQuote, 400), [state]);

  const handleSwitchConversion = () => {
    if (state.tradeType === "buy") {
      let newState: any = { tradeType: "sell" };
      let list = [];

      // filter non-sellable cryptos
      list = allCurrencies.current.crypto.filter((crypto: CryptoCurrencyType) => crypto.canSell);
      newState.filteredCryptoCurrencies = list;

      // if selected crypto is not supported, get first selection in the list
      if (!state.selectedCrypto.canSell) {
        newState.selectedCrypto = list.find((crypto: CryptoCurrencyType) => crypto.canSell);
      }

      // filter non-sellable fiat
      list = allCurrencies.current.fiat.filter((fiat: FiatCurrencyType) => fiat.canSell);
      newState.filteredFiatCurrencies = list;

      // if selected fiat is not supported, get first selection in the list
      if (!state.selectedFiat.canSell) {
        newState.selectedFiat = list.find((fiat: FiatCurrencyType) => fiat.canSell);
        newState.selectedPaymentMethod = newState.selectedFiat.paymentMethods[0];

        setInputAmount(state => {
          return {
            ...state,
            fiat: newState.selectedFiat.paymentMethods[0].max,
          }
        });

        const previousCurrency = state.selectedFiat.label;
        setUnsupportedConversion(state => {
          return {
            ...state,
            hasError: true,
            currency: previousCurrency,
          }
        });
      } else {
        setUnsupportedConversion({
          hasError: false,
          currency: "",
          count: 0,
        });
      }

      setState(state => {
        return {
          ...state,
          ...newState,
        };
      });
    } else {
      setState(state => {
        return {
          ...state,
          tradeType: "buy",
          filteredFiatCurrencies: allCurrencies.current.fiat,
          filteredCryptoCurrencies: allCurrencies.current.crypto,
        };
      });
    }
  }

  return (
    <>
      <Modal
        show={showModal}
        toggle={toggleModal}
      >
        <span className="font-bold block text-2xl mb-4">👋 Hi there!</span>
        <p>This is a simple exercise to integrate Xanpool and Coingecko APIs.</p>
        <p className="mt-4">Site optimization (cache API result, etc...) isn't taken into account during the development, which I should, well, maybe in the next enhancement... 🙃</p>
        <p className="mt-4">You can have a peek of the source codes <a className="text-blue-500" href="https://gitlab.com/pugkutteh/xanpool-playground" rel="noreferrer" target="_blank">here</a> and please feel free to <a className="text-blue-500" href="mailto:pugkutteh@gmail.com">leave a feedback</a> and help me to improve for the better 🎉</p>
        <p className="mt-4">Cheers!</p>
      </Modal>

      <Card classes="h-full p-4">
        {unsupportedConversion.hasError ? <div className="px-3 py-2 mb-4 bg-yellow-100 text-yellow-700 text-sm rounded-lg border border-yellow-400">{state.selectedCrypto.label} can't be converted to {unsupportedConversion.currency}.</div> : <></>}

        <div className="relative">
          <StepTitle
            stepNumber="1"
            label={state.tradeType === "buy" ? "Convert fiat" : "Sell crypto"}
          />

          {state.tradeType === "buy" && <FormGroup
            id="fiat"
            options={state.filteredFiatCurrencies}
            selectedOption={state.selectedFiat}
            value={inputAmount.fiat}
            min={state.selectedPaymentMethod.min}
            max={state.selectedPaymentMethod.max}
            shouldValidate={true}
            isLoading={isLoading}
            placeholder={`${state.selectedPaymentMethod.min} - ${state.selectedPaymentMethod.max}`}
            onInputChange={value => handleInputChange("fiat", value)}
            onDropdownChange={handleDropdownChange("fiat")}
          />}

          {state.tradeType === "sell" && <FormGroup
            id="crypto"
            options={state.filteredCryptoCurrencies}
            selectedOption={state.selectedCrypto}
            value={inputAmount.crypto}
            isLoading={isLoading}
            placeholder="0.00"
            onInputChange={value => handleInputChange("crypto", value)}
            onDropdownChange={handleDropdownChange("crypto")}
          />}

          <SwitchConversionButton onClick={handleSwitchConversion} />
        </div>

        <hr className="my-4" />

        <div>
          <StepTitle
            stepNumber="2"
            label={state.tradeType === "buy" ? "Receive crypto ≈" : "Receive fiat ≈"}
          />

          {state.tradeType === "sell" && <FormGroup
            id="fiat"
            options={state.filteredFiatCurrencies}
            selectedOption={state.selectedFiat}
            value={inputAmount.fiat}
            min={state.selectedPaymentMethod.min}
            max={state.selectedPaymentMethod.max}
            shouldValidate={true}
            isLoading={isLoading}
            placeholder={`${state.selectedPaymentMethod.min} - ${state.selectedPaymentMethod.max}`}
            onInputChange={value => handleInputChange("fiat", value)}
            onDropdownChange={handleDropdownChange("fiat")}
          />}

          {state.tradeType === "buy" && <FormGroup
            id="crypto"
            options={state.filteredCryptoCurrencies}
            selectedOption={state.selectedCrypto}
            value={inputAmount.crypto}
            isLoading={isLoading}
            placeholder="0.00"
            onInputChange={value => handleInputChange("crypto", value)}
            onDropdownChange={handleDropdownChange("crypto")}
          />}
        </div>

        <hr className="my-4" />

        <div>
          <StepTitle
            stepNumber="3"
            label={state.tradeType === "buy" ? "Pay with" : "Transfer to"}
          />

          <Dropdown
            id="payment-methods"
            options={state.selectedFiat.paymentMethods}
            selectedOption={state.selectedPaymentMethod}
            placeholder="Payment Method"
            onChange={handlePaymentMethodChange}
          ></Dropdown>
        </div>

        <hr className="my-4" />

        <Button
          classes="w-full"
          label="Continue"
          icon={<ArrowNarrowRightIcon />}
          iconPosition="right"
          onClick={() => toggleModal(true)}
        />
      </Card>
    </>
  )
}

export default ConversionForm;