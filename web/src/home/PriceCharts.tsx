import { useEffect, useState } from "react";
import axios from "axios";
import Skeleton from "react-loading-skeleton";
import PriceChartItem from "./PriceChartItem";
import { PriceChartType } from "models/PriceChart";
import Card from "components/Card";

const PriceCharts = () => {
  const [currencies, setCurrencies] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getPriceCharts();
  }, []);

  function getPriceCharts() {
    axios.post('/api/cryptocurrencies/market-data')
      .then(response => {
        setCurrencies(response.data);
        setIsLoading(false);
      });
  }

  return (
    <div className="grid grid-cols-1 sm:grid-cols-3 gap-4">
      {!!isLoading && [...Array(5)].map(value => (
        <Card key={value}>
          <div className="p-3">
            <div><Skeleton width="40%" /></div>
            <Skeleton width="65%" />
            <Skeleton height={38} />
          </div>
        </Card>
      ))}

      {!isLoading && currencies.map((currency: PriceChartType) =>
        <PriceChartItem
          key={currency.ticker}
          currency={currency}
        ></PriceChartItem>
      )}
    </div>
  )
}

export default PriceCharts;