import Header from "components/Header";
import Container from "components/Container";
import ConversionForm from "./ConversionForm/Index";
import PriceCharts from "./PriceCharts";

const Home = () => {
  return (
    <>
      <Header></Header>

      <Container classes="my-8">
        <h3 className="text-white text-4xl">Convert your assets<br />via local payment method</h3>

        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 mt-8">
          <div>
            <ConversionForm></ConversionForm>
          </div>

          <div className="lg:col-span-2">
            <PriceCharts></PriceCharts>
          </div>
        </div>
      </Container>
    </>
  )
}

export default Home;