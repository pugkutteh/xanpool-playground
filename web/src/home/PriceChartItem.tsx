import NumberFormat from "react-number-format";
import { Line } from "react-chartjs-2";
import { PriceChartType } from "models/PriceChart";
import Card from "../components/Card";

type Props = {
  currency: PriceChartType,
};

const chartOptions: object = {
  plugins: {
    legend: {
      display: false,
    },
    tooltip: {
      enabled: false,
    },
  },
  scales: {
    x: {
      grid: {
        display: false,
        drawBorder: false,
        tickLength: 0,
      },
      ticks: {
        display: false,
      },
    },
    y: {
      grid: {
        display: false,
        drawBorder: false,
        tickLength: 0,
      },
      ticks: {
        display: false,
      },
    },
  },
};

const PriceChartItem = ({ currency }: Props) => {
  const latestPrice = currency.marketData.prices[currency.marketData.prices.length - 1] ?? 0;

  const chartData = (canvas: HTMLCanvasElement) => {
    const ctx = canvas.getContext("2d");
    var gradientStroke = ctx?.createLinearGradient(0, 0, 0, 50);
    gradientStroke?.addColorStop(0, "#6f65e9");
    gradientStroke?.addColorStop(1, "#ffffff");

    return {
      labels: currency.marketData.timestamp,
      datasets: [
        {
          data: currency.marketData.prices,
          backgroundColor: gradientStroke,
          borderColor: "#4b2091",
          borderWidth: 1,
          pointRadius: 0,
          tension: 0.4,
          fill: {
            target: "origin",
            below: "rgb(0, 0, 255)"
          },
        },
      ],
    };
  }

  return (
    <Card classes="overflow-hidden">
      <div className="p-3 pb-0">
        <div className="text-xs">{currency.ticker}</div>
        <NumberFormat
          className="text-xl font-medium text-primary"
          value={latestPrice}
          thousandSeparator={true}
          displayType="text"
          decimalScale={currency.decimalScale ?? 2}
        />
      </div>

      <Line
        data={chartData}
        options={chartOptions}
        height={80}
      ></Line>
    </Card>
  )
}

export default PriceChartItem;