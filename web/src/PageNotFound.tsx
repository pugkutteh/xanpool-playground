import Lottie from 'react-lottie';
import notFoundLottie from './images/404-error.json';
import Header from 'components/Header';

const PageNotFound = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: notFoundLottie,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  return (
    <>
      <Header></Header>

      <div className="h-screen flex items-center justify-center">
        <div className="max-w-lg">
          <Lottie
            options={defaultOptions}
            height={400}
            width={400}
          />
        </div>
      </div>
    </>
  )
}

export default PageNotFound;