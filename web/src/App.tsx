import { Switch, Route } from "react-router-dom";
import HomePage from "./home/Page";
import NotFoundPage from "./PageNotFound";

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={HomePage}></Route>
      <Route component={NotFoundPage}></Route>
    </Switch>
  );
}

export default App;
