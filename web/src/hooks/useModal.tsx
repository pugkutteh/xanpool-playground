import { useState } from "react";

const useModal = () => {
  const [showModal, setShowModal] = useState(false);

  const toggleModal = (flag: boolean) => {
    setShowModal(flag);
  }

  return {
    showModal,
    toggleModal,
  };
};

export default useModal;