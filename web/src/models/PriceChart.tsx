export type PriceChartType = {
  ticker: string,
  decimalScale: number,
  marketData: {
    prices: number[],
    timestamp: number[],
  },
}