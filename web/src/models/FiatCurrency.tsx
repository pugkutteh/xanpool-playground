export type FiatCurrencyType = {
  code: string
  label: string
  canSell: boolean,
  paymentMethods: [],
}