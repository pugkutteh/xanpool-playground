export type DropdownOptionType = {
  code: string,
  label: string,
};