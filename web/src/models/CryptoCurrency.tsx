export type CryptoCurrencyType = {
  code: string,
  label: string,
  canSell: boolean,
  decimalScale: number,
}