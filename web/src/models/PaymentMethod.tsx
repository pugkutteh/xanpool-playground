export type PaymentMethodType = {
  code: string,
  label: string,
  min: number | undefined,
  max: number | undefined,
}